# Capacitor Conekta
Capacitor plugin for Conketa

### Install
```bash
npm install capacitor-conekta
```

### Web
Register the plugin by importing it.
```ts
import "capacitor-conekta";
```

### Android
In file `android/app/src/main/AndroidManifest.xml` add the following under `<application></application>`:
```xml
<uses-library android:name ="org.apache.http.legacy" android:required ="false"/>
```
Register the plugin inside your `android/app/src/main/java/**/**/MainActivity.java` file

```java
import lat.bit.plugin.capacitor.conekta.ConektaClient;
[...]
this.init(savedInstanceState, new ArrayList<Class<? extends Plugin>>() {
      {
        [...]
        add(ConektaClient.class);
        [...]
      }
    });
```

### Use it

```ts
await Conekta.setPublicKey(<your conekta public key>);
const token = await Conekta.createToken({
  cardNumber: '',
  name: '',
  expYear: '',
  expMonth: '',
  cvc: '',
});
```