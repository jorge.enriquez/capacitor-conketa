import Foundation
import Capacitor
/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitor.ionicframework.com/docs/plugins/ios
 */
@objc(ConektaClient)
public class ConektaClient: CAPPlugin {
    private let conekta = Conekta()

    @objc func setPublicKey(_ call: CAPPluginCall) {
        let publicKey = call.getString("publicKey") ?? ""
        conekta.publicKey = publicKey
        call.success()
    }

    @objc func createToken(_ call: CAPPluginCall) {
        let cardNumber = call.getString("cardNumber") ?? ""
        let name = call.getString("name") ?? ""
        let expMonth = call.getString("expMonth") ?? ""
        let expYear = call.getString("expYear") ?? ""
        let cvc = call.getString("cvc") ?? ""
        DispatchQueue.main.sync {
            self.conekta.collectDevice()
        }
        
        let card = conekta.card()
        card?.setNumber(cardNumber, name: name, cvc: cvc, expMonth: expMonth, expYear: expYear)
        let token = conekta.token()
        token?.card = card
        token?.create(success: { (data) -> Void in
            if let data = data as NSDictionary? as! [String:Any]? {
                let token = data["id"] as? String
                call.resolve([
                    "token": token
                ])
            }
        }, andError: { (error) -> Void in
            //print(error)
        })
    }
}
