package lat.bit.plugin.capacitor.conekta;

import android.util.Log;
import android.app.Activity;
import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import io.conekta.conektasdk.Conekta;
import io.conekta.conektasdk.Card;
import io.conekta.conektasdk.Token;
import org.json.JSONObject;

@NativePlugin()
public class ConektaClient extends Plugin {

  @PluginMethod()
  public void setPublicKey(PluginCall call) {
    String publicKey = call.getString("publicKey");
    Conekta.setPublicKey(publicKey);
    call.success();
  }

  @PluginMethod()
  public void createToken(PluginCall call) {
    Log.d(getLogTag(), call.getString("cardNumber", "41"));
    try {
      String name = call.getString("name");
      String cardNumber = call.getString("cardNumber");
      String expMonth = call.getString("expMonth");
      String expYear = call.getString("expYear");
      String cvc = call.getString("cvc");
      final Activity activity = getActivity();
      final PluginCall localCall = call;
      final Card card = new Card(name, cardNumber, cvc, expMonth, expYear);
      activity.runOnUiThread(new Runnable() {
        @Override
        public void run() {
          Conekta.collectDevice(activity);
          final Token token = new Token(activity);
          token.onCreateTokenListener(new Token.CreateToken() {
            @Override
            public void onCreateTokenReady(JSONObject data) {
              try {
                Log.d(getLogTag(), data.toString());
                String responseType = data.getString("object");
                if (responseType.equals("token")) {
                  Log.d(getLogTag(), data.getString("id"));
                  JSObject response = new JSObject();
                  response.put("token", data.getString("id"));
                  localCall.success(response);
                } else {
                  String message = data.getString("message_to_purchaser");
                  localCall.reject(message);
                }
              } catch (Exception err) {
                Log.d(getLogTag(), "Error");
                Log.d(getLogTag(), err.toString());
                localCall.reject(err.toString());
              }
            }
          });
          token.create(card);
        }
      });
    } catch (Exception err) {
      call.reject(err.toString());
    }
  }
}
