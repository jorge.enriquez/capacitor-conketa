import { WebPlugin } from "@capacitor/core";
import { ConektaClientPlugin } from "./definitions";
import { registerWebPlugin } from "@capacitor/core";

declare const Conekta: any;

export class ConektaClientWeb extends WebPlugin implements ConektaClientPlugin {
  sdkLoaded = false;

  constructor() {
    super({
      name: "ConektaClient",
      platforms: ["web"],
    });
  }

  addConektaSdk(): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        const conektaUrl = "https://cdn.conekta.io/js/latest/conekta.js";
        const script = document.createElement("script");
        script.type = "text/javascript";
        script.src = conektaUrl;
        script.async = true;
        document.body.appendChild(script);
        script.addEventListener("load", () => {
          this.sdkLoaded = true;
          resolve();
        });
      } catch (err) {
        reject(err);
      }
    });
  }

  async setPublicKey(options: { publicKey: string }): Promise<void> {
    if (!this.sdkLoaded) {
      await this.addConektaSdk();
    }
    Conekta.setPublicKey(options.publicKey);
    return;
  }

  async createToken(options: {
    name: string;
    cardNumber: string;
    expMonth: string;
    expYear: string;
    cvc: string;
  }): Promise<{ token: string }> {
    if (!this.sdkLoaded) {
      await this.addConektaSdk();
    }
    const token = await this.tokenizeCard(options);
    return { token };
  }

  tokenizeCard(options: {
    name: string;
    cardNumber: string;
    expMonth: string;
    expYear: string;
    cvc: string;
  }): Promise<string> {
    return new Promise((resolve, reject) => {
      const tokenParams = {
        card: {
          number: options.cardNumber,
          name: options.name,
          exp_year: options.expYear,
          exp_month: options.expMonth,
          cvc: options.cvc,
        },
      };
      Conekta.Token.create(
        tokenParams,
        (token: { id: string }) => {
          resolve(token.id);
        },
        (err: any) => {
          reject(err);
        }
      );
    });
  }
}

const ConektaClient = new ConektaClientWeb();

export { ConektaClient };

registerWebPlugin(ConektaClient);
