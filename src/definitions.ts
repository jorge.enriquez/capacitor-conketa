declare module "@capacitor/core" {
  interface PluginRegistry {
    ConektaClient: ConektaClientPlugin;
  }
}

export interface ConektaClientPlugin {
  setPublicKey(options: { publicKey: string }): Promise<void>;
  createToken(options: {
    name: string;
    cardNumber: string;
    expMonth: string;
    expYear: string;
    cvc: string;
  }): Promise<{ token: string }>;
}
